using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HutongGames.PlayMaker;

namespace UnityEngine.Video
{
    [ActionCategory(ActionCategory.Video)]
    [NoActionTargets]
    public class VideoPlayerCrossFade : FsmStateAction
    {
        [ObjectType(typeof(VideoPlayer))]
        public FsmGameObject From = new FsmGameObject();
        [ObjectType(typeof(VideoPlayer))]
        public FsmGameObject To = new FsmGameObject();

        public FsmAnimationCurve Curve = new FsmAnimationCurve();
        public FsmEvent FinishEvent = new FsmEvent(string.Empty);

        private VideoPlayer from;
        private VideoPlayer to;
        private float time;
        private float endTime;

        public override void OnEnter()
        {
            if (From.Value is object && !From.Value.Equals(null))
                From.Value.TryGetComponent(out from);
            if (To.Value is object && !To.Value.Equals(null))
                To.Value.TryGetComponent(out to);
            time = Curve.curve.keys[0].time;
            endTime = Curve.curve.keys[^1].time;

            Execute();
        }
        public override void OnUpdate()
        {
            Execute();
        }
        public void Execute()
        {
            if (time >= endTime)
            {
                Finish();
                return;
            }

            var e = Curve.curve.Evaluate(time);
            if (from is object && !from.Equals(null))
            {
                from.targetCameraAlpha = 1f - e;
                from.SetDirectAudioVolume(0, 1f - e);
            }
            if (to is object && !to.Equals(null))
            {
                to.targetCameraAlpha = e;
                to.SetDirectAudioVolume(0, e);
            }

            time += Time.deltaTime;
        }
        public new void Finish()
        {
            base.Finish();
            if (!FsmEvent.IsNullOrEmpty(FinishEvent))
                Fsm.Event(FinishEvent);
        }
    }
}