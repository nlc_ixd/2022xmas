using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HutongGames.PlayMaker;

namespace UnityEngine.Video
{
    [ActionCategory(ActionCategory.Video)]
    [ActionTarget(typeof(VideoPlayerLoop), "OwnerDefault")]
    public class VideoPlayerLoopFade : FsmStateAction
    {
        [RequiredField]
        public FsmOwnerDefault OwnerDefault = new FsmOwnerDefault();

        public FsmAnimationCurve Curve = new FsmAnimationCurve();
        public FsmEvent FinishEvent = new FsmEvent(string.Empty);

        private float time;
        private float endTime;
        private VideoPlayerLoop component;
        public void GetComponent()
        {
            GameObject g = Fsm.GetOwnerDefaultTarget(OwnerDefault);
            if (g is null || g.Equals(null))
                return;
            g.TryGetComponent(out component);
        }
        public override void OnEnter()
        {
            GetComponent();
            time = Curve.curve.keys[0].time;
            endTime = Curve.curve.keys[^1].time;
        }
        public override void OnUpdate()
        {
            Execute();
        }
        public void Execute()
        {
            if (time > endTime)
            {
                Finish();
                return;
            }

            var e = Curve.curve.Evaluate(time);
            component.Alpha = e;

            time += Time.deltaTime;
        }
        public new void Finish()
        {
            base.Finish();
            if (!FsmEvent.IsNullOrEmpty(FinishEvent))
                Fsm.Event(FinishEvent);
        }
    }
    [ActionCategory(ActionCategory.Video)]
    [ActionTarget(typeof(VideoPlayerLoop), "OwnerDefault")]
    public class VideoPlayerFade : FsmStateAction
    {
        [RequiredField]
        public FsmOwnerDefault OwnerDefault = new FsmOwnerDefault();

        public FsmAnimationCurve Curve = new FsmAnimationCurve();
        public FsmEvent FinishEvent = new FsmEvent(string.Empty);

        private float time;
        private float endTime;
        private VideoPlayer component;
        public void GetComponent()
        {
            GameObject g = Fsm.GetOwnerDefaultTarget(OwnerDefault);
            if (g is null || g.Equals(null))
                return;
            g.TryGetComponent(out component);
        }
        public override void OnEnter()
        {
            GetComponent();
            time = Curve.curve.keys[0].time;
            endTime = Curve.curve.keys[^1].time;
        }
        public override void OnUpdate()
        {
            Execute();
        }
        public void Execute()
        {
            if (time > endTime)
            {
                Finish();
                return;
            }

            var e = Curve.curve.Evaluate(time);
            component.targetCameraAlpha = e;
            component.SetDirectAudioVolume(0, e);

            time += Time.deltaTime;
        }
        public new void Finish()
        {
            base.Finish();
            if (!FsmEvent.IsNullOrEmpty(FinishEvent))
                Fsm.Event(FinishEvent);
        }
    }
    [ActionCategory(ActionCategory.Video)]
    [ActionTarget(typeof(VideoPlayerLoop), "OwnerDefault")]
    public class VideoPlayerLoopPause : FsmStateAction
    {
        [RequiredField]
        public FsmOwnerDefault OwnerDefault = new FsmOwnerDefault();

        private VideoPlayerLoop component;
        public void GetComponent()
        {
            GameObject g = Fsm.GetOwnerDefaultTarget(OwnerDefault);
            if (g is null || g.Equals(null))
                return;
            g.TryGetComponent(out component);
        }
        public override void OnEnter()
        {
            GetComponent();
            component.Current.Pause();
        }
    }
    [ActionCategory(ActionCategory.Video)]
    [ActionTarget(typeof(VideoPlayerLoop), "OwnerDefault")]
    public class VideoPlayerLoopPlay : FsmStateAction
    {
        [RequiredField]
        public FsmOwnerDefault OwnerDefault = new FsmOwnerDefault();

        private VideoPlayerLoop component;
        public void GetComponent()
        {
            GameObject g = Fsm.GetOwnerDefaultTarget(OwnerDefault);
            if (g is null || g.Equals(null))
                return;
            g.TryGetComponent(out component);
        }
        public override void OnEnter()
        {
            GetComponent();
            component.Current.targetCamera = component.Current.targetCamera;
            component.Current.frame = 0;
            component.Current.Play();
            Finish();
        }
    }
}