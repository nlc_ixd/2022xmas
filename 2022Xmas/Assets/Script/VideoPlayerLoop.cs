using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace UnityEngine.Video
{
    public class VideoPlayerLoop : MonoBehaviour
    {
        [Header("Reference")]
        public VideoPlayer PlayerA;
        public VideoPlayer PlayerB;
        public RawImage Image;

        [Header("Data")]
        public bool PlayOnStart;
        public float Alpha;

        [Header("ReadOnly")]
        public long Frame;
        public ulong FrameCount;
        public float Progress;

        public bool IsA
        {
            get;
            private set;
        } = true;
        public VideoPlayer Current
        {
            get => IsA ? PlayerA : PlayerB;
        }
        public VideoPlayer Other
        {
            get => IsA ? PlayerA : PlayerB;
        }

        public void SetAlpha(VideoPlayer player, float a)
        {
            var isA = ReferenceEquals(player, PlayerA);
            if (isA) 
            {
                PlayerA.targetCameraAlpha = a;
                PlayerA.SetDirectAudioVolume(0, a);
            }
            else
            {
                PlayerB.targetCameraAlpha = a;
                PlayerB.SetDirectAudioVolume(0, a);
            }
        }
        public void PlayOther(VideoPlayer player)
        {
            IsA = !ReferenceEquals(player, PlayerA);
            
            SetAlpha(player, 0f);
            player.loopPointReached -= PlayOther;
            player.Stop();
            player.Prepare();

            SetAlpha(Other, 0f);
            Other.loopPointReached += PlayOther;
            Other.Play();

            FrameCount = Other.frameCount;
        }

        private void Start()
        {
            if (PlayOnStart)
            {
                Current.Play();
                Current.loopPointReached += PlayOther;
                FrameCount = Current.frameCount;

                Other.Prepare();
            }
        }

        private void Update()
        {
            Frame = Current.frame;
            Progress = (float)Frame / FrameCount;
            if (Progress > 0.5f)
                Other.Prepare();

            SetAlpha(Current, Alpha);
            SetAlpha(Other, Alpha);
            var c = Image.color;
            c.a = Alpha;
            Image.color = c;
        }
    }
}